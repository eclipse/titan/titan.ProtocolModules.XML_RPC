/*******************************************************************************
* Copyright (c) 2000-2023 Ericsson Telecom AB
*
* XSD to TTCN-3 Translator version: CRL 113 200/6 R4B                       
*
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
*******************************************************************************/
//
//  File:               XML_RPC_test.ttcn
//  Description:        XML_RPC encoding-decoding test
//  Rev:                <RnXnn>
//  Prodnr:             
//  Updated:            
//  Contact:            
///////////////////////////////////////////////
module XML_RPC_test {


import from XML_RPC  all;

type component Test_CT {
}

template MethodResponse t_GetVoucherHistoryResponse := {
  choice := {
    params := {
      param_ := {
        value_ := {
          embed_values:={"",""},
          choice := {
            struct := {
              member_list := {
                {
                  name := "responseCode",
                  value_ := {
                    embed_values:={"",""},
                    choice := {
                      i4 := 0
                    }
                  }
                },
                {
                  name := "agent",
                  value_ := {
                    embed_values:={"",""},
                    choice := {
                      string := "BZ101"
                    }
                  }
                },
                {
                  name := "batchId",
                  value_ := {
                    embed_values:={"",""},
                    choice := {
                      string := "ABC1234"
                    }
                  }
                },
                {
                  name := "currency",
                  value_ := {
                    embed_values:={"",""},
                    choice := {
                      string := "EUR"
                    }
                  }
                },
                {
                  name := "expiryDate",
                  value_ := {
                    embed_values:={"",""},
                    choice := {
                      dateTime_iso8601 := "2003-07-17T00:00:00"
                    }
                  }
                },
                {
                  name := "voucherGroup",
                  value_ := {
                    embed_values:={"",""},
                    choice := {
                      string := "A1"
                    }
                  }
                },
                {
                  name := "state",
                  value_ := {
                    embed_values:={"",""},
                    choice := {
                      i4 := 2
                    }
                  }
                },
                {
                  name := "value",
                  value_ := {
                    embed_values:={"",""},
                    choice := {
                      string := "10000"
                    }
                  }
                },
                {
                  name := "extensionText1",
                  value_ := {
                    embed_values:={"",""},
                    choice := {
                      string := "VoucherType=1,Market=Europe"
                    }
                  }
                },
                {
                  name := "extensionText2",
                  value_ := {
                    embed_values:={"",""},
                    choice := {
                      string := "19990817T00:00:00"
                    }
                  }
                },
                {
                  name := "extensionText3",
                  value_ := {
                    embed_values:={"",""},
                    choice := {
                      string := "42"
                    }
                  }
                },
                {
                  name := "transactionRecords",
                  value_ := {
                    embed_values:={"",""},
                    choice := {
                      array := {
                        data := {
                          value_list := {
                            {
                              embed_values:={"",""},
                              choice := {
                                struct := {
                                  member_list :=
                                  {
                                    {
                                      name := "operatorId",
                                      value_ := {
                                        embed_values:={"",""},
                                        choice := {

                                          string := "administrator"
                                        }
                                      }
                                    },
                                    {
                                      name := "newState",
                                      value_ := {
                                        embed_values:={"",""},
                                        choice := {

                                          i4 := 5
                                        }
                                      }
                                    },
                                    {
                                      name := "timestamp",
                                      value_ := {
                                        embed_values:={"",""},
                                        choice := {

                                          dateTime_iso8601 := "2002-10-09T02:39:55"
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            {
                              embed_values:={"",""},
                              choice := {
                                struct := {
                                  member_list :=
                                  {
                                    {
                                      name := "operatorId",
                                      value_ := {
                                        embed_values:={"",""},
                                        choice := {

                                          string := "administrator"
                                        }
                                      }
                                    },
                                    {
                                      name := "newState",
                                      value_ := {
                                        embed_values:={"",""},
                                        choice := {

                                          i4 := 0
                                        }
                                      }
                                    },
                                    {
                                      name := "timestamp",
                                      value_ := {
                                        embed_values:={"",""},
                                        choice := {

                                          dateTime_iso8601 := "2002-10-10T01:39:54"
                                        }
                                      }
                                    }
                                  }
                                }
                              }}
                            ,
                            {
                              embed_values:={"",""},
                              choice := {

                                struct := {
                                  member_list :=
                                  {
                                    {
                                      name := "subscriberId",
                                      value_ := {
                                        embed_values:={"",""},
                                        choice := {

                                          string := "706691616"
                                        }
                                      }
                                    },
                                    {
                                      name := "timestamp",
                                      value_ := {
                                        embed_values:={"",""},
                                        choice := {

                                          dateTime_iso8601 := "2002-10-10T01:39:54"
                                        }
                                      }
                                    },
                                    {
                                      name := "transactionId",
                                      value_ := {
                                        embed_values:={"",""},
                                        choice := {

                                          string := "123456"
                                        }
                                      }
                                    },
                                    {
                                      name := "newState",   // newState put at the end
                                      value_ := { 
                                        embed_values:={"",""},
                                        choice := {
                                          // to test whether SET OF works
                                          i4 := 1
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}


template MethodResponse t_GetVoucherHistoryResponse1 := {
  choice := {
    params := {
      param_ := {
        value_ := {
          embed_values:={"",""},
          choice:= {
            struct := {
              member_list := {
                {
                  name := "responseCode",
                  value_ := {
                    embed_values:={"",""},
                    choice:= {
                      i4 := 0
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}



template MethodCall t_DeleteTask_SingleParam :=
{
  order:={methodName, params},
  methodName := "DeleteTask",
  params := {
    param_list := {{
        value_ := {
          embed_values:={"",""},
          choice := {
            struct := {
              member_list := {
                {
                  name := "taskId",
                  value_ := {
                    embed_values:={"",""},
                    choice:= {

                      i4 := 44
                    }
                  }
                }}
            }
          }
        }
      }
    }
  }
}


template MethodCall/*_MultipleParams */ t_GetGenerateVoucherUsageReportTaskInfo :=
{   
  order:={methodName, params},
  methodName := "GetGenerateVoucherUsageReportTaskInfo",
  params := {
    param_list := {
      {
        value_ := {
          embed_values:={"",""},
          choice := {
            struct := {
              member_list := { {
                  name := "originNodeType",
                  value_ := {
                    embed_values:={"",""},
                    choice:= {
                      string := "IVR"
                    }}
                }}
            }
          }
        }
      }
    }
  }

}


template MethodCall t_GeneralUpdate_SingleParam := {
   order:={methodName, params},
  methodName := "GeneralUpdate",
  params := {
    param_list := { {
        value_ := {
          embed_values:={"",""},
          choice := {
            struct := {
              member_list := {
                {
                  name := "originNodeType",
                  value_ := {
                    embed_values:={"",""},
                    choice:= {
                      string := "IVR"
                    }}
                },
                {
                  name := "originHostName",
                  value_ := {
                    embed_values:={"",""},
                    choice:= {
                      string := "ws00824"
                    }}
                },
                {
                  name := "originTransactionID",
                  value_ := {
                    embed_values:={"",""},
                    choice:= {
                      string := "31349729532086766516"
                    }}
                },
                {
                  name := "originTimeStamp",
                  value_ := {
                    embed_values:={"",""},
                    choice:= {
                      dateTime_iso8601 := "2012-02-06T13:19:00"
                    }
                  }
                },
                {
                  name := "subscriberNumber",
                  value_ := {
                    embed_values:={"",""},
                    choice:= {
                      string := "4558024013"
                    }}
                },
                {
                  name := "transactionCurrency",
                  value_ := {
                    embed_values:={"",""},
                    choice:= {
                      string := "SEK"
                    }}
                },
                {
                  name := "offerUpdateInformationList",
                  value_ := {
                    embed_values:={"",""},
                    choice:= {
                      array := {
                        data := {
                          value_list := {
                            {
                              embed_values:={"",""},
                              choice:= {
                                struct := {
                                  member_list := {
                                    {
                                      name := "offerID",
                                      value_ := {
                                        embed_values:={"",""},
                                        choice:= {
                                          i4 := 20// int := 20
                                        }
                                      }
                                    },
                                    {
                                      name := "startDate",
                                      value_ := {
                                        embed_values:={"",""},
                                        choice:= {
                                          dateTime_iso8601 := "2012-02-07T12:00:00+00:00"
                                        }
                                      }
                                    },
                                    {
                                      name := "expiryDate",
                                      value_ := {
                                        embed_values:={"",""},
                                        choice:= {
                                          dateTime_iso8601 := "2012-02-10T12:00:00+00:00"
                                        }
                                      }
                                    },
                                    {
                                      name := "offerType",
                                      value_ := {
                                        embed_values:={"",""},
                                        choice:= {
                                          i4 := 0//  int := 0
                                        }
                                      }
                                    },
                                    {
                                      name := "dedicatedAccountUpdateInformation",
                                      value_ := {
                                        embed_values:={"",""},
                                        choice:= {
                                          array := {
                                            data := {
                                              value_list := {
                                                {
                                                  embed_values:={"",""},
                                                  choice:= {
                                                    struct := {
                                                      member_list := {
                                                        {
                                                          name := "dedicatedAccountID",
                                                          value_ := {
                                                            embed_values:={"",""},
                                                            choice:= {
                                                              i4 := 1//  int := 1
                                                            }
                                                          }
                                                        },
                                                        {
                                                          name := "dedicatedAccountValueNew",
                                                          value_ := {
                                                            embed_values:={"",""},
                                                            choice:= {
                                                              string := "100"
                                                            }
                                                          }
                                                        },
                                                        {
                                                          name := "dedicatedAccountUnitType",
                                                          value_ := {
                                                            embed_values:={"",""},
                                                            choice:= {
                                                              i4 := 1// int := 1
                                                            }
                                                          }
                                                        }
                                                      }
                                                    }
                                                  }
                                                }
                                              }
                                            }
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }

}

testcase tc_TestEncDec() runs on Test_CT {

log (t_GetVoucherHistoryResponse);
var MethodResponse pdu := valueof(t_GetVoucherHistoryResponse);
var octetstring encodedPdu;

encodedPdu := enc_MethodResponse(pdu);
log(encodedPdu);




log(dec_MethodResponse(encodedPdu))


if (match(dec_MethodResponse(encodedPdu), t_GetVoucherHistoryResponse)) {
setverdict(pass);
} else {
setverdict(fail);
}


log (t_DeleteTask_SingleParam);
var MethodCall pdu2 := valueof(t_DeleteTask_SingleParam);

var octetstring encodedPdu2;

encodedPdu2 := enc_MethodCall(pdu2);
log(encodedPdu2);

log(dec_MethodCall(encodedPdu2))
if (match(dec_MethodCall(encodedPdu2), t_DeleteTask_SingleParam)) {
setverdict(pass);
} else {
setverdict(fail);
}


log (t_GetGenerateVoucherUsageReportTaskInfo);
var MethodCall pdu3 := valueof(t_GetGenerateVoucherUsageReportTaskInfo);
var octetstring encodedPdu3;

encodedPdu3 := enc_MethodCall(pdu3);
log(encodedPdu3);

log(dec_MethodCall(encodedPdu3))
if (match(dec_MethodCall(encodedPdu3), t_GetGenerateVoucherUsageReportTaskInfo)) {
setverdict(pass);
} else {
setverdict(fail);
}

log (t_GeneralUpdate_SingleParam );
var MethodCall pdu4 := valueof(t_GeneralUpdate_SingleParam );

var octetstring encodedPdu4;

encodedPdu4 := enc_MethodCall(pdu4);
log(encodedPdu4);

log(dec_MethodCall(encodedPdu4))
if (match(dec_MethodCall(encodedPdu4), t_GeneralUpdate_SingleParam )) {
setverdict(pass);
} else {
setverdict(fail);
}

}


control {
    execute(tc_TestEncDec())
}

}
